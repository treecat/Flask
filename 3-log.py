记录应用错误
    错误邮件
    
        ADMINS = ['yourname@example.com']
        if not app.debug:
            import logging
            from logging.handlers import SMTPHandler
            mail_handler = SMTPHandler('127.0.0.1', 'server-error@example.com', ADMINS, 'YourApplication Failed')
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)
            创建了一个新的 SMTPHandler 来用监听 127.0.0.1 的邮件服务器向所有的 ADMINS 发送发件人为 server-error@example.com ，主题为 “YourApplication Failed” 的邮件。
        
    记录到文件
        FileHandler - 在文件系统上记录日志
        RotatingFileHandler - 在文件系统上记录日志， 并且当消息达到一定数目时，会滚动记录
        NTEventLogHandler - 记录到 Windows 系统中的系统事件日志。如果你在 Windows 上做开发，这就是你想要用的。
        SysLogHandler - 发送日志到 Unix 的系统日志
        
        if not app.debug:
            import logging
            from themodule import TheHandlerYouWant
             file_handler = TheHandlerYouWant(...)
            file_handler.setLevel(logging.WARNING)
            app.logger.addHandler(file_handler)