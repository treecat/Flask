创建应用上下文
	显式地调用 app_context() 方法:

	from flask import Flask, current_app

	app = Flask(__name__)
	with app.app_context():
    	# within this block, current_app points to app.
    	print current_app.name
	在配置了 SERVER_NAME 时，应用上下文也被用于 url_for() 函数。这允许你在没有请求时生成 URL 。




应用上下文局部变量
	应用上下文会在必要时被创建和销毁。它不会在线程间移动，并且也不会在不同的请求之间共享。正因为如此，它是一个存储数据库连接信息或是别的东西的最佳位置。
	内部的栈对象叫做 flask._app_ctx_stack 。
	扩展可以在最顶层自由地存储额外信息，想象一下它们用一个充分独特的名字在那里存储信息，而不是在 flask.g 对象里， flask.g 是留给用户的代码用的。