配置基础
    config 实际上继承于字典，并且可以像修改字典一样修改它:
    
    app = Flask(__name__)
    app.config['DEBUG'] = True
    
从文件配置
    app = Flask(__name__)
    app.config.from_object('yourapplication.default_settings')
    app.config.from_envvar('YOURAPPLICATION_SETTINGS')
    首先从 yourapplication.default_settings 模块加载配置，然后用 YOURAPPLICATION_SETTINGS 环境变量指向的文件的内容覆盖其值.
    
    在 shell 中用 export 命令设置:

    $ export YOURAPPLICATION_SETTINGS=/path/to/settings.cfg
    $ python run-app.py
    * Running on http://127.0.0.1:5000/
    * Restarting with reloader...
    在 Windows 下则使用其内置的 set 命令:

    >set YOURAPPLICATION_SETTINGS=\path\to\settings.cfg
    这里是一个配置文件的例子:

    # Example configuration
    DEBUG = False
    SECRET_KEY = '?\xbf,\xb4\x8d\xa3"<\x9c\xb0@\x0f5\xab,w\xee\x8d$0\x13\x8b83'
    
    
开发 / 生产
class Config(object):
    DEBUG = False
    TESTING = False
    DATABASE_URI = 'sqlite://:memory:'

class ProductionConfig(Config):
    DATABASE_URI = 'mysql://user@localhost/foo'

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True
启用这样的配置你需要调用 from_object()

app.config.from_object('configmodule.ProductionConfig')