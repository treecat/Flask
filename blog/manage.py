from app import create_app, db, split
from app.models import User
from flask.ext.script import Manager, Shell
from jinja2.environment import Environment
import os


app = create_app(os.getenv('FLASK_CONFIG') or 'default')
env=app.jinja_env
env.filters['split'] = split

manager = Manager(app)

def make_shell_context():
	return dict(app=app, db=db, User=User)


manager.add_command("shell", Shell(make_context=make_shell_context))

if __name__ == '__main__':
	manager.run()
