#coding:utf-8

from datetime import datetime
from flask import render_template, request, session, redirect, url_for
from flask import jsonify
from flask import flash
from flask.ext.login import login_required
from flask.ext.login import login_user
from flask.ext.login import current_user
from flask.ext.login import logout_user
from . import admin
from .. import db
from ..models import User
from ..models import Category
from ..models import Post
from .forms import PostForm

@admin.route('/admin/', methods=['GET', 'POST'])
def index():
	'''if session.get('username'):
		categories = Category.query.all()
		return render_template('admin_index.html', username=session.get('username'), categories=categories)
	return render_template('admin_login.html')
	'''
	if current_user.is_authenticated:
		categories = Category.query.all()
		
		posts = Post.query.all()
		year_month_post_dict = {}
		for post in posts:
			key = post.post_time.strftime('%Y-%m')
			if post.post_time.strftime('%Y-%m') not in year_month_post_dict:
				year_month_post_dict[key] = 0
			year_month_post_dict[key] = year_month_post_dict[key] + 1
		return render_template('admin_index.html', username=current_user.name, categories=categories, year_month_post_dict=year_month_post_dict)
	return render_template('admin_login.html')

@admin.route('/admin/login', methods=['POST'])
def login():
	username = request.form['username']
	user = User.query.filter_by(name=username).first()
	if user is not None:
		login_user(user)
		# session['username'] = username
		return "0"
	return "1"

@admin.route('/admin/logout')
def logout():
	logout_user()
	return redirect(url_for('.index'))

@admin.route('/admin/category/<id>')
@login_required
def category(id):
	category = Category.query.filter(Category.id == id).first()
	return render_template('admin_category.html', category=category)

@admin.route('/admin/category/add', methods=['POST', 'GET'])
@login_required
def category_add():
	if request.method == 'GET':
		return render_template('admin_add_category.html')
	category_name = request.form['name']
	if Category.query.filter(Category.name == category_name).first():
		return "已存在同名类别!"
	category = Category(name=category_name)
	db.session.add(category)
	db.session.commit()
	return "0"

@admin.route('/admin/category/delete/<id>')
@login_required
def category_delete(id):
	category = Category.query.filter_by(id=id).first()
	if category:
		db.session.delete(category)
	return "0"

@admin.route('/admin/post/<year_month>')
def category_year_month(year_month):
	posts = Post.query.filter(Post.post_time <= year_month+'-30').filter(Post.post_time >= year_month+'-01')
	return render_template('admin_post.html', year_month=year_month, posts=posts)

@admin.route('/admin/post/write', methods=['GET', 'POST'])
@login_required
def post_write():
	form = PostForm(user=current_user)
	if form.validate_on_submit():
		if form.id.data:
			post = Post.query.filter_by(id=form.id.data).first()
			post.title = form.title.data
			post.content = form.content.data
			post.category = Category.query.get(form.category.data)
			db.session.add(post)
			db.session.commit()
			flash('更新成功!')
			# year_month存在说明是从时间轴过来的，应该返回时间轴
			if form.year_month.data:
				return redirect(url_for('.category_year_month', year_month=form.year_month.data))
			# 返回类别
			return redirect(url_for('.category', id=form.category_id.data))
		else:
			post = Post()
			post.title = form.title.data
			post.content = form.content.data
			post.category = Category.query.get(form.category.data)
			post.user = form.user
			db.session.add(post)
			db.session.commit()
			flash('发布成功!')
			return redirect(url_for('.post_write'))
	return render_template('admin_post_write.html', form=form)

# 在时间轴里修改
@admin.route('/admin/post/update/<id>/<year_month>')
@login_required
def post_update(id, year_month):
	form = PostForm(user=current_user)
	post = Post.query.filter_by(id=id).first()
	form.id.data = post.id
	form.year_month.data = year_month
	form.title.data = post.title
	form.content.data = post.content
	form.category.data = post.category.id
	return render_template('admin_post_write.html', form=form)

# 在类别里修改
@admin.route('/admin/post/update/<id>')
@login_required
def post_update_category(id):
	form = PostForm(user=current_user)
	post = Post.query.filter_by(id=id).first()
	form.id.data = post.id
	form.category_id.data = post.category.id
	form.title.data = post.title
	form.content.data = post.content
	form.category.data = post.category.id
	return render_template('admin_post_write.html', form=form)


