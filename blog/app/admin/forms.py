from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, SelectField, SubmitField, HiddenField
from wtforms.validators import Required
from ..models import Category


class PostForm(Form):
	id = HiddenField('id:')
	year_month = HiddenField('year_month:')
	category_id = HiddenField('category_id:')
	title = StringField('title:', validators=[Required()])
	content = TextAreaField()
	category = SelectField('category:', coerce=int)
	submit = SubmitField('Submit')

	def __init__(self, user, *args, **kwargs):
		super(PostForm, self).__init__(*args, **kwargs)
		self.category.choices = [(category.id, category.name) for category in Category.query.all()]
		self.user = user