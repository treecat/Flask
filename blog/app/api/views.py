from flask import jsonify
from . import api
from ..models import Category

@api.route('/categories/')
def get_categories():
	categories = Category.query.all()
	return jsonify({'categories': [category.to_json() for category in categories] })