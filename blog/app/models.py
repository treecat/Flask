from flask import url_for
from flask.ext.login import UserMixin
from markdown import markdown
import bleach
from . import login_manager
from datetime import datetime
from . import db


class User(UserMixin, db.Model):
	__tablename__ = 'user'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True)
	email = db.Column(db.String(64), unique=True)
	password = db.Column(db.String(64))
	last_seen = db.Column(db.DateTime(), default=datetime.now)
	posts = db.relationship('Post', backref='user', lazy='dynamic')
	
	def to_json(self):
		json_user = {
			'url' : url_for('api.get_post', id=self.id, _external=True),
			'username' : self.name,
			'last_seen' : self.last_seen,
			'post_count' : self.posts.count()
		}
		return json_user

	@staticmethod
	def insertUser():
		user = User.query.filter_by(name='jc').first()
		if user is None:
			user = User(name='jc', email='jc@jc.com')
			db.session.add(user)
			db.session.commit()
	
	@login_manager.user_loader
	def load_user(user_id):
		return User.query.get(int(user_id))
	
	def __repr__(self):
		return '<User %r>' % self.name

class Post(db.Model):
	__tablename__ = 'post'
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	content = db.Column(db.Text)
	post_time = db.Column(db.DateTime(), index=True, default=datetime.now)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
	comments = db.relationship('Comment', backref='post', lazy='dynamic')

	@staticmethod
	def insertPost():
		author = User.query.all()[0]
		category = Category.query.all()[0]
		post = Post(title='how to solve it?', content='xxxxxxxxxxxxxxxx', user=author, category=category)
		db.session.add(post)
		db.session.commit()
	
	def __repr__(self):
		return '<Post %r>' % self.title


class Category(db.Model):
	__tablename__ = 'category'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True)
	posts = db.relationship('Post', backref='category', lazy='dynamic')
	
	def to_json(self):
		json_category = {
			'url' : url_for('api.get_categories', id=self.id, _external=True),
			'id' : self.id,
			'name' : self.name,
			'post_count' : len(self.posts)
		}
		return json_category

	@staticmethod
	def insertCategory():
		category = Category.query.filter_by(name='java thinking').first()
		if category is None:
			category = Category(name='java thinking')
			db.session.add(category)
			db.session.commit()

	def __repr__(self):
		return '<Category %r>' % self.name

class Comment(db.Model):
	__tablename__ = 'comment'
	id = db.Column(db.Integer, primary_key=True)
	content = db.Column(db.Text)
	content_html = db.Column(db.Text)
	timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
	post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
	
	@staticmethod
	def on_change_content(target, value, oldvalue, initiator):
		allowed_tags = ['a', 'abbr', 'acronym', 'b', 'code', 'em', 'i', 'strong', 'br']
		target.content_html = bleach.linkify(bleach.clean(markdown(value, output_format='html'), tags=allowed_tags, strip=True))


db.event.listen(Comment.content, 'set', Comment.on_change_content)







