from datetime import datetime
from flask import render_template, session, redirect, url_for, request
from . import main
from .. import db
from ..models import User, Category, Post, Comment
from .forms import CommentForm

@main.route('/', methods=['GET', 'POST'])
def index():
	# get all category
	categories = Category.query.all()
	page = request.args.get('page', 1, type=int)
	pagination = Post.query.order_by(Post.post_time.desc()).paginate(page, per_page=7, error_out=False)
	posts = pagination.items
	# get top 7 posts
	recent_posts = Post.query.order_by(Post.post_time.desc()).limit(7)
	# timeline
	all_posts = Post.query.all()
	year_month_post_dict = {}
	for post in all_posts:
		key = post.post_time.strftime('%m/%Y')
		if post.post_time.strftime('%m/%Y') not in year_month_post_dict:
			year_month_post_dict[key] = 0
		year_month_post_dict[key] = year_month_post_dict[key] + 1
	
	return render_template('index.html', categories=categories, pagination=pagination, posts=posts, recent_posts=recent_posts, year_month_post_dict=year_month_post_dict)

@main.route('/view/post/<int:id>', methods=['GET'])
def view_post(id):
	# 固定的部分
	categories = Category.query.all()
	recent_posts = Post.query.order_by(Post.post_time.desc()).limit(7)
	# 需要查找的
	post = Post.query.filter_by(id=id).first()
	# 当前点击类别的Id
	click_category_id = request.args.get('categoryId',0, type=int)
	# 分页参数
	page = request.args.get('page', 1, type=int)
	pagination = Comment.query.filter_by(post=post).paginate(page, per_page=7, error_out=False)
	# timeline
	all_posts = Post.query.all()
	year_month_post_dict = {}
	for p in all_posts:
		key = p.post_time.strftime('%m/%Y')
		if p.post_time.strftime('%m/%Y') not in year_month_post_dict:
			year_month_post_dict[key] = 0
		year_month_post_dict[key] = year_month_post_dict[key] + 1
	
	form = CommentForm()
	form.postId.data = post.id
	return render_template('view.html', form=form, pagination=pagination, year_month_post_dict=year_month_post_dict, categories=categories, post=post, recent_posts=recent_posts, click_category_id=click_category_id)


@main.route('/view/category/<id>')
def view_category(id):
	#固定的部分
	categories = Category.query.all()
	recent_posts = Post.query.order_by(Post.post_time.desc()).limit(7)
	click_category = Category.query.filter_by(id=id).first()
	# 分页参数
	page = request.args.get('page', 1, type=int)
	pagination = click_category.posts.order_by(Post.post_time.desc()).paginate(page, per_page=7, error_out=False)
	# timeline
	all_posts = Post.query.all()
	year_month_post_dict = {}
	for post in all_posts:
		key = post.post_time.strftime('%m/%Y')
		if post.post_time.strftime('%m/%Y') not in year_month_post_dict:
			year_month_post_dict[key] = 0
		year_month_post_dict[key] = year_month_post_dict[key] + 1

	return render_template('view_category.html', click_category_id=click_category.id, pagination=pagination, year_month_post_dict=year_month_post_dict, categories=categories, recent_posts=recent_posts)


@main.route('/view/timeline/<int:month>/<int:year>')
def view_timeline(month, year):
	#固定的部分
	categories = Category.query.all()
	recent_posts = Post.query.order_by(Post.post_time.desc()).limit(7)
	# timeline
	all_posts = Post.query.all()
	year_month_post_dict = {}
	for post in all_posts:
		key = post.post_time.strftime('%m/%Y')
		if post.post_time.strftime('%m/%Y') not in year_month_post_dict:
			year_month_post_dict[key] = 0
		year_month_post_dict[key] = year_month_post_dict[key] + 1

	d1 = datetime(year, month, 1, 0, 0, 0 ,0)
	d2 = datetime(year, month, 30, 23, 0, 0 ,0)
	# 分页参数
	page = request.args.get('page', 1, type=int)
	pagination = Post.query.filter(Post.post_time>=d1).filter(Post.post_time<=d2).order_by(Post.post_time.desc()).paginate(page, per_page=7, error_out=False)

	return render_template('view_timeline.html', categories=categories, pagination=pagination, recent_posts=recent_posts, year_month_post_dict=year_month_post_dict, month=month, year=year)

@main.route('/comment/add', methods=['POST'])
def add_comment():
	post = Post.query.filter_by(id=request.form['postId']).first()
	comment = Comment()
	comment.content = request.form['content']
	comment.post = post
	db.session.add(comment)
	db.session.commit()
	return redirect(url_for('.view_post', id=request.form['postId']))


