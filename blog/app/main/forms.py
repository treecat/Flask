from flask.ext.pagedown.fields import PageDownField
from flask.ext.wtf import Form
from wtforms.validators import Required
from wtforms import SubmitField, TextAreaField, HiddenField

class CommentForm(Form):
	postId = HiddenField('postId')
	content = TextAreaField("what's on your mind?", validators=[Required()])
	submit = SubmitField('Submit')

