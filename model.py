
标准上下文

    下面的全局变量默认在 Jinja2 模板中可用:

    config
        当前的配置对象 (flask.config)

    request
        当前的请求对象 (flask.request)。当模版不是在活动的请求上下文中渲染时这个变量不可用。
        
    session
        当前的会话对象 (flask.session)。
        
    g
        请求相关的全局变量 (flask.g)。

    url_for()
        flask.url_for() 函数

    get_flashed_messages()
        flask.get_flashed_messages() 函数
       
       
        
标准过滤器
    tojson()
        这个函数把给定的对象转换为 JSON 表示，
        注意 script 标签里的东西不应该被转义，因此如果你想在 scr>ipt 标签里使用它， 请使用 |safe 来禁用转义，:
        <script type=text/javascript
            doSomethingWith({{ user.username|tojson|safe }});
        </script>


控制自动转义
    有三种可行的解决方案:
    在传递到模板之前，用 Markup 对象封装 HTML字符串。一般推荐这个方法。
    在模板中，使用 |safe 过滤器显式地标记一个字符串为安全的 HTML （ {{ myvariable|safe }} ）。
    临时地完全禁用自动转义系统。
    在模板中禁用自动转义系统，可以使用 {%autoescape %} 块:

    {% autoescape false %}
        <p>autoescaping is disabled here
        <p>{{ will_not_be_escaped }}
    {% endautoescape %}
    
    
注册过滤器
    jinja_env 或者使用 template_filter() 装饰器。
    下面两个例子作用相同，都是反转一个对象:
    @app.template_filter('reverse')
    def reverse_filter(s):
        return s[::-1]

    def reverse_filter(s):
        return s[::-1]
    app.jinja_env.filters['reverse'] = reverse_filter
    注册之后， 你可以在模板中像使用 Jinja2 内置过滤器一样使用你的过滤器，例如你在上下文中有一个名为 mylist 的 Python 列表:
    {% for x in mylist | reverse %}
    {% endfor %}
    
    
    
上下文处理器
    上下文处理器自动向模板的上下文中插入新变量。上下文处理器在模板渲染之前运行，并且可以在模板上下文中插入新值.返回字典的函数，这个字典的键值最终将传入应用中所有模板的上下文:
    @app.context_processor
    def inject_user():
        return dict(user=g.user)
    变量不仅限于值，上下文处理器也可以使某个函数在模板中可用（由于 Python 允许传递函数）:
    @app.context_processor
    def utility_processor():
        def format_price(amount, currency=u'€'):
            return u'{0:.2f}{1}.format(amount, currency)
        return dict(format_price=format_price)
    上面的上下文处理器使得 format_price 函数在所有模板中可用:

    {{ format_price(0.33) }}



